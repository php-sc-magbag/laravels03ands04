@extends('layouts.app')

@section('content')
    <h1>Create Post</h1>
    <form action="/posts" method="post">
        @csrf
        <div class="form-group">
            <label for="title">Title</label>
            <input type="text" class="form-control" name="title">

         
        </div> 
        <div class="form-group">
                <label for="title">Body</label>
                <textarea class="form-control" placeholder="body" name="body" rows="10" id="body"></textarea>
        </div>
        <br>
        <button class="btn btn-primary" type="submit">Submit</button>
        </div>



    </form>

    <script>
ClassicEditor
.create( document.querySelector( '#body' ) )
.catch( error => {
console.error( error );
} );
</script>
@endsection